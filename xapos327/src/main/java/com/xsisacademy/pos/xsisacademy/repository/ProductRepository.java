package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	
	//Menggunakan JpaRepository
	List<Product> findByIsActive(Boolean isActive);
	
	//Contoh Menggunakan 2 paramater cth pakai JAVA CLASS
	@Query(value = "select p from Product p where p.isActive = ?1 and p.productInitial like '%?2%' order by p.productName")
	List<Product> findByIsActiveAndInitial(Boolean isActive, String productInitial);
	
	
	//Contoh Menggunakan Join table
	@Query(value = "select * from product p join variant v on p.variant_id = v.id "
			+ "where p.is_active = true and v.category_id = ?1", nativeQuery = true)
	List<Product> findByCategoryId(Long categoryId);
	

}
