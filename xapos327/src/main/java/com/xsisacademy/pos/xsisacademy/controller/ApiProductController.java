package com.xsisacademy.pos.xsisacademy.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.Product;
import com.xsisacademy.pos.xsisacademy.model.VMProduct;
import com.xsisacademy.pos.xsisacademy.model.Variant;
import com.xsisacademy.pos.xsisacademy.repository.ProductRepository;
import com.xsisacademy.pos.xsisacademy.repository.VariantRepository;

@RestController
@RequestMapping("/api/")
public class ApiProductController {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private VariantRepository variantRepository;
	
	//public static String uploadDirectory = System.getProperty("user.dir") + "/imagedata";
	public static String uploadDirectory = System.getProperty("user.dir") + "/src/main/resources/static/imagedata";

	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllProduct(){
		try {
			List<Product> listProduct = this.productRepository.findByIsActive(true);
			return new ResponseEntity<>(listProduct, HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("product/variantbycategoryid/{categoryId}")
	public ResponseEntity<List<Variant>> getVariantByCategoryId(@PathVariable("categoryId") Long categoryId){
		try {
			List<Variant> listVariant = this.variantRepository.findByCategoryId(categoryId);
			return new ResponseEntity<>(listVariant, HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	/*@PostMapping("product/add")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product){
		
		product.createBy = "admin1";
		product.createDate = new Date();
		
		Product productData = this.productRepository.save(product);
		
		if(productData.equals(product)) {
			return new ResponseEntity<>("Save Data Success", HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
		
	}*/
	
	@PostMapping("product/add")
	public ResponseEntity<Object> saveProduct(VMProduct vmproduct){
		
		LocalDateTime datetime = LocalDateTime.now();
		DateTimeFormatter formatdate = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		
		String filename = String.valueOf(datetime.format(formatdate)) + "_" + vmproduct.file.getOriginalFilename();
		Path fileNameAndPath = Paths.get(uploadDirectory, filename);
		try {
			Files.write(fileNameAndPath, vmproduct.file.getBytes());
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
		Product product = new Product();
		product.variantId = vmproduct.variantId;
		product.productInitial = vmproduct.productInitial;
		product.productName = vmproduct.productName;
		product.description = vmproduct.description;
		product.price = vmproduct.price;
		product.stock = vmproduct.stock;
		product.sphoto = filename;
		//product.isActive = vmproduct.isActive; //kalau checkboxnya disabled maka isActive nya jadi null jadi error
		product.isActive = true;
		product.createBy = "admin1";
		product.createDate = new Date();
		
		Product productData = this.productRepository.save(product);
		
		if(productData.equals(product)) {
			return new ResponseEntity<>(product, HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Save Failed", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@GetMapping("product/{id}")
	public ResponseEntity<Object> getProductById(@PathVariable Long id){
		try {
			Optional<Product> product = this.productRepository.findById(id);
			return new ResponseEntity<>(product, HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("product/edit/{id}")
	public ResponseEntity<Object> editProduct(@PathVariable("id") Long id, @RequestBody Product product){
		
		Optional<Product> productData = this.productRepository.findById(id);
		
		if(productData.isPresent()) {
			product.id = id;
			product.modifyBy = "admin1";
			product.modifyDate = new Date();
			product.createBy = productData.get().createBy;
			product.createDate = productData.get().createDate;
			
			this.productRepository.save(product);
			return new ResponseEntity<>("Updated Data Success", HttpStatus.OK);
		}
		else {
			return ResponseEntity.notFound().build();
		}
		
	}
	
	@PutMapping("product/delete/{id}")
	public ResponseEntity<Object> deleteProduct(@PathVariable("id") Long id){
		Optional<Product> productData = this.productRepository.findById(id);
		
		if(productData.isPresent()) {
			Product product = new Product();
			product.id = id;
			product.isActive = false;
			product.modifyBy ="admin1";
			product.modifyDate = new Date();
			product.createBy = productData.get().createBy;
			product.createDate = productData.get().createDate;
			product.productInitial = productData.get().productInitial;
			product.productName = productData.get().productName;
			product.variantId = productData.get().variantId;
			product.description = productData.get().description;
			product.price = productData.get().price;
			product.stock = productData.get().stock;
			
			this.productRepository.save(product);
			return new ResponseEntity<>("Deleted Data Success", HttpStatus.OK);
			
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
	
	
}
