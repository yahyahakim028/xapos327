package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Xapos327MailApplication {

	public static void main(String[] args) {
		SpringApplication.run(Xapos327MailApplication.class, args);
	}

}
